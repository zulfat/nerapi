from flask import Flask, request, abort, jsonify
from NERecognizer import NERecognizer
from newspaper import fulltext
import requests

app = Flask(__name__)
ner = NERecognizer()
proxies = []
current_proxy_id = 0


def get_html(url):
    proxy = None
    return requests.get(url, proxies=proxy).text


@app.route('/url', methods=['POST'])
def extract_entities_url():
    if not request.json or 'url' not in request.json:
        abort(400)
    html = get_html(request.json['url'])
    text = fulltext(html)
    entities = ner.extract_entities_from_text(text)
    return jsonify(entities), 201


@app.route('/text', methods=['POST'])
def extract_entities_text():
    if not request.json or 'text' not in request.json:
        abort(400)
    text = request.json['text']
    entities = ner.extract_entities_from_text(text)
    return jsonify(entities), 201


if __name__ == '__main__':
    app.run(threaded=True, host="0.0.0.0")
