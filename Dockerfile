FROM ubuntu:18.04
MAINTAINER Zulfat Miftakhutdinov

RUN apt-get update && apt-get -y install python3 python3-pip && \
    pip3 install flask newspaper3k spacy && \
    python3 -m spacy download en_core_web_md && \
    mkdir nerapi

COPY *.py /nerapi/
ENTRYPOINT ["python3", "/nerapi/api.py"]
