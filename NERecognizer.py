import spacy
import re


class NERecognizer(object):

    def __init__(self):
        self.nlp = spacy.load('en_core_web_md')
        self.entity_types = ['PERSON', 'GPE', 'NORP', 'ORG', 'LOC', 'PRODUCT', 'EVENT', 'LAW']

    def extract_entities_from_text(self, txt):
        cleaned_text = self.clean_text(txt)
        document = self.nlp(cleaned_text)
        entities = []
        for entity in document.ents:
            if entity.label_ not in self.entity_types: continue
            entities.append({'text': entity.text, 'type': entity.label_})
        return entities

    def clean_text(self, txt):
        cleaned_text = txt
        cleaned_text = re.sub('[\n]+', ' ', cleaned_text)
        cleaned_text = re.sub('[ ]+', ' ', cleaned_text)
        return cleaned_text
